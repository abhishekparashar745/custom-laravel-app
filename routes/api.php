<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'auth:api'], function () {

	Route::get('user-detail', 'App\Http\Controllers\API\RegisterController@userDetails');

	Route::get('logout', 'App\Http\Controllers\API\RegisterController@logoutApi');
});

Route::post('register', 'App\Http\Controllers\API\RegisterController@userRegister');

Route::post('user-login', 'App\Http\Controllers\API\RegisterController@login');
