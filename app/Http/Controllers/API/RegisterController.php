<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class RegisterController extends BaseController
{

      /**
     * @return json
     * @Url: /api/userRegister/
     * @Method: POST
     * @Parameters   
     *      name: required
     *      username: required
     *      email: required
     *      password: required
     *      confirm_password: required
     */
    public function userRegister(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|regex:/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}.*$/',
            'confirm_password' => 'required|same:password'
        ],[
            'password.regex' => 'New Password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters'
        ]);

        
        if( $validator->fails() ) {
            foreach ($validator->errors()->all() as $error){
                $error_msg[] = $error;    
            }
            $error = implode(", ",$error_msg);
            return $this->sendError('Validation Error.', $error);
        }

        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $input['name'] = $input['name'];
        $input['email'] = $input['email'];

        $user = User::create($input); 
    
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        $success['email'] =  $user->email;

        return $this->sendResponse($success, "You've registered successfully.");
    }


    /**
	 * @return json
	 * @Url: /api/user-login/
	 * @Method: POST
	 * @Parameters   
	 *  	username: required
	 *  	password: required
	 */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        
        if( $validator->fails() ) {
            foreach ($validator->errors()->all() as $error){
                $error_msg[] = $error;    
            }
            $error = implode(", ",$error_msg);
            return $this->sendError('Validation Error.', $error);
        }
        
        $credentials = request(['email', 'password']);
        
        if(!Auth::attempt($credentials)){

            return $this->sendError('Validation Error.', 'Unauthorized.');
        }
        
        $user_data = Auth::user(); 

        $user = $request->user();
        $tokenResult = $user->createToken('MyApp');
        
        $success['access_token'] =  $tokenResult->accessToken; 
        $success['token_type'] =  'Bearer'; 
        $success['email'] =  $user->email;
            
        return $this->sendResponse($success, 'You have logged in successfully.');
            
    }

	/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function userDetails(Request $request) 
    { 
        $user = Auth::user(); 
        
        $user = User::where('email',$user->email)->first();
        return $this->sendResponse($user, 'User Details.');
    } 

    public function logoutApi(Request $request)
    { 
      $token = $request->user()->token();
      $token->revoke();

      return $this->sendResponse(null, 'You have been successfully logged out!');
    }

}
